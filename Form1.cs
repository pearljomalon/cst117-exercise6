using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Exercise6
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void buttonClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonRollDice_Click(object sender, EventArgs e)
        {
            Dice leftDie = new Dice(12);
            Dice rightDie = new Dice(6);
            bool snakeEyes = false;

            int leftDigit = 0;
            int rightDigit = 0;

            while (!snakeEyes)
            {
                textDie1.Text = ("1");
                textDie2.Text = ("1");

                leftDigit = leftDie.rollDice();
                rightDigit = rightDie.rollDice();

                if ((leftDigit == rightDigit) && (leftDigit ==1))
                {
                    snakeEyes = true;
                    MessageBox.Show(string.Format("You got snake eyes in {0} tries.", leftDie.numberOfRolls));
                }
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}
