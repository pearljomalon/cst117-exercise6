namespace Exercise6
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonRollDice = new System.Windows.Forms.Button();
            this.textDie1 = new System.Windows.Forms.TextBox();
            this.textDie2 = new System.Windows.Forms.TextBox();
            this.buttonClose = new System.Windows.Forms.Button();
            this.rollDiceLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // buttonRollDice
            // 
            this.buttonRollDice.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.25F);
            this.buttonRollDice.Location = new System.Drawing.Point(12, 53);
            this.buttonRollDice.Name = "buttonRollDice";
            this.buttonRollDice.Size = new System.Drawing.Size(200, 200);
            this.buttonRollDice.TabIndex = 0;
            this.buttonRollDice.Text = "Roll Dice";
            this.buttonRollDice.UseVisualStyleBackColor = true;
            this.buttonRollDice.Click += new System.EventHandler(this.buttonRollDice_Click);
            // 
            // textDie1
            // 
            this.textDie1.BackColor = System.Drawing.Color.Black;
            this.textDie1.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F);
            this.textDie1.ForeColor = System.Drawing.Color.White;
            this.textDie1.Location = new System.Drawing.Point(228, 53);
            this.textDie1.Multiline = true;
            this.textDie1.Name = "textDie1";
            this.textDie1.Size = new System.Drawing.Size(100, 100);
            this.textDie1.TabIndex = 1;
            this.textDie1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textDie2
            // 
            this.textDie2.BackColor = System.Drawing.Color.Black;
            this.textDie2.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F);
            this.textDie2.ForeColor = System.Drawing.Color.White;
            this.textDie2.Location = new System.Drawing.Point(348, 53);
            this.textDie2.Multiline = true;
            this.textDie2.Name = "textDie2";
            this.textDie2.Size = new System.Drawing.Size(100, 100);
            this.textDie2.TabIndex = 2;
            this.textDie2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // buttonClose
            // 
            this.buttonClose.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.buttonClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F);
            this.buttonClose.ForeColor = System.Drawing.Color.Black;
            this.buttonClose.Location = new System.Drawing.Point(228, 194);
            this.buttonClose.Name = "buttonClose";
            this.buttonClose.Size = new System.Drawing.Size(220, 59);
            this.buttonClose.TabIndex = 3;
            this.buttonClose.Text = "Close";
            this.buttonClose.UseVisualStyleBackColor = true;
            this.buttonClose.Click += new System.EventHandler(this.buttonClose_Click);
            // 
            // rollDiceLabel
            // 
            this.rollDiceLabel.AutoSize = true;
            this.rollDiceLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.rollDiceLabel.Location = new System.Drawing.Point(8, 9);
            this.rollDiceLabel.Name = "rollDiceLabel";
            this.rollDiceLabel.Size = new System.Drawing.Size(141, 20);
            this.rollDiceLabel.TabIndex = 4;
            this.rollDiceLabel.Text = "Please roll the dice";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(460, 265);
            this.Controls.Add(this.rollDiceLabel);
            this.Controls.Add(this.buttonClose);
            this.Controls.Add(this.textDie2);
            this.Controls.Add(this.textDie1);
            this.Controls.Add(this.buttonRollDice);
            this.Name = "Form1";
            this.Text = "Dice Game";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonRollDice;
        private System.Windows.Forms.TextBox textDie1;
        private System.Windows.Forms.TextBox textDie2;
        private System.Windows.Forms.Button buttonClose;
        private System.Windows.Forms.Label rollDiceLabel;
    }
}

