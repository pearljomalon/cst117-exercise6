using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise6
{
    class Dice
    {
        Random genRandNumb = new Random();
        public int numberOfSides { get; set; }
        public int numberOfRolls { get; set; }

        public Dice()
        {
            numberOfSides = 6;
        }

        public int rollDice()
        {
            numberOfRolls++;
            return genRandNumb.Next(1, numberOfSides + 1);
        }

        public Dice(int numberOfSides)
        {
            this.numberOfSides = numberOfSides;
        }
    }
}